﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lives : MonoBehaviour {

    public static int lives = 10;

	public static void SetLives(int lives)
    {
        Lives.lives -= lives;
    }

    public static int GetLives()
    {
        return lives;
    }
}

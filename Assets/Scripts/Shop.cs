﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shop : MonoBehaviour
{

    
    public GameObject[] turrets;

    private GameObject turretToBuild;
    

    public void BuyStandardTurret()
    {
        turretToBuild = turrets[0];
        if (Money.GetMoney() >= turretToBuild.GetComponent<Tower>().towerCosts)
        {
            BuildManager.buildManager.SetTurretToBuild(turrets[0]);
        }
        else
        {
            BuildManager.buildManager.SetTurretToBuild(null);
        }
    }

    public void BuyMissileTurret()
    {
        turretToBuild = turrets[1];
        if (Money.GetMoney() >= turretToBuild.GetComponent<Tower>().towerCosts)
        {
            BuildManager.buildManager.SetTurretToBuild(turrets[1]);
        }
        else
        {
            BuildManager.buildManager.SetTurretToBuild(null);
        }
    }

    public void DeleteTower()
    {
        Destroy(BuildManager.buildManager.GetSelectedTower(), 0f);
    }

    public void UpgradeTower()
    {
        if(Money.GetMoney()>=BuildManager.buildManager.GetSelectedTower().GetComponent<Tower>().UpgradeCosts)
        {
            Money.SetMoney(-BuildManager.buildManager.GetSelectedTower().GetComponent<Tower>().UpgradeCosts);
            BuildManager.buildManager.GetSelectedTower().GetComponent<Tower>().Upgrade();
        }
    }
}

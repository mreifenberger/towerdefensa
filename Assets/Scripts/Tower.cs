﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{

    //target finding
    [Header("Targetting Attributes")]
    public float renewRate = 0.4f;
    public float turnspeed = 5f;
    public float range = 20f;

    [Header("Targetting Objects")]
    public Transform target;
    public Transform partToRotate;
    public string[] enemyTags;

    //Shooting
    [Header("Shoot Attributes")]
    public float fireRate = 1f;
    public float fireCountdown = 0f;
    public int towerDamage = 20;
    public float bulletSpeed = 0f;
    public float explosionRadius = 0;

    [Header("Upgrade Attributes")]
    public float fireRateMultiplicator = 1;
    public int towerDamageMultiplicator = 2;
    public float explosionRadiusMultiplicator = 0;
    public float rangeAddicator = 1;

    public Color color;
    int towerLevel=1;
    int bottomUpgradeCount=0;
    int topUpgradeCount=0;

    [Header("Shoot Objects")]
    public GameObject bulletPrefab;
    public Transform firepoint;
    public GameObject shootParticle;

    [Header("Shop Attributes")]
    public int towerCosts;
    public int UpgradeCosts;

    // Use this for initialization
    void Start()
    {
        InvokeRepeating("UpdateTarget", 0f, renewRate);
    }

    void UpdateTarget()
    {

        float shortestDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;
        foreach (string tag in enemyTags)
        {
            GameObject[] enemies = GameObject.FindGameObjectsWithTag(tag);
            foreach (GameObject enemy in enemies)
            {
                float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
                if (distanceToEnemy < shortestDistance)
                {
                    shortestDistance = distanceToEnemy;
                    nearestEnemy = enemy;
                }
            }



            if (nearestEnemy != null && shortestDistance <= range)
            {
                target = nearestEnemy.transform;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (target == null)
        {
            return;
        }

        Vector3 dir = target.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(dir);
        Vector3 rotation = Quaternion.Lerp(partToRotate.rotation, lookRotation, Time.deltaTime * turnspeed).eulerAngles;
        partToRotate.rotation = Quaternion.Euler(0f, rotation.y, 0f);

        if (fireCountdown <= 0f)
        {
            Shoot();
            fireCountdown = 1f / fireRate;
        }

        fireCountdown -= Time.deltaTime;

    }

    void Shoot()
    {
        GameObject spawnedBullet = (GameObject)Instantiate(bulletPrefab, firepoint.position, firepoint.rotation);
        Bullet bullet = spawnedBullet.GetComponent<Bullet>();
        PassValues(bullet);
        GameObject shootEffect = (GameObject)Instantiate(shootParticle, firepoint.position, firepoint.rotation);
        shootEffect.transform.parent = this.firepoint.transform;
        Destroy(shootEffect, 0.3f);

        if (bullet != null)
        {
            bullet.Seek(target);
        }
    }

    void PassValues(Bullet bullet)
    {
        bullet.BulletDamage = towerDamage;
        bullet.EnemyTags = enemyTags;
        bullet.BulletSpeed = bulletSpeed;
        bullet.ExplosionRadius = explosionRadius;
    }

    public void Upgrade()
    {
        fireRate = fireRate * fireRateMultiplicator;
        towerDamage = towerDamage * towerDamageMultiplicator;
        explosionRadius = explosionRadius + explosionRadiusMultiplicator;
        topUpgradeCount++;
        if (topUpgradeCount == 5)
        {
            bottomUpgradeCount = topUpgradeCount;
            topUpgradeCount = 0;
        }
        towerLevel++;
    }

    public int GetTowerLevel()
    {
        return towerLevel;
    }

    public void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }

}

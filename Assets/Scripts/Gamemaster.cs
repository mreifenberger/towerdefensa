﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class Gamemaster : MonoBehaviour
{

    public Transform[] enemies;
    public int[] possibilty;
    bool found = false;

    private float waittime = 2f;

    public Transform spawnPoint;

    public Button[] button;

    public Text lives;
    public Text moneyUI;
    public Text roundUI;
    public Text losetext;

    public Button btnNextWave;

    public float timeBetweenWaves = 5f;
    public float timeBetweenEnemies = .5f;

    private Button endButton;


    const int MinimumUnits = 5;
    int level = 0;

    void Start()
    {
        losetext.text = "";
        //enemies = new Transform[transform.childCount];        
        possibilty = new int[enemies.Length];
        for (int i = 0; i < possibilty.Length; i++)
        {
            if (i == 0)
            {
                possibilty[i] = 100;
            }
            else
            {
                possibilty[i] = 0;
            }

            //enemies[i] = transform.GetChild(i);
        }
        SetTextUI();
    }

    void SetEnemyPossibility()
    {
        //looks for the first value != 0 and sets it -5 and the value after it +5
        for (int i = 0; i < possibilty.Length; i++)
        {
            if (possibilty[i] != 0 && !found && i != possibilty.Length - 1)
            {
                possibilty[i] -= 5;
                possibilty[i + 1] += 5;
                found = true;
            }
        }
        found = false;
    }

    public void SpawnNextWave()
    {
        timeBetweenWaves = 0f;
        Checktime();
    }

    private void CheckPlayerHP()
    {
        if (Lives.GetLives() < 0)
        {
            losetext.text = "YOU LOST";
            Application.Quit();
            waittime -= Time.deltaTime;
            if (waittime <= 0)
            {
                Application.LoadLevel("Main_Menu");
            }
        }
    }

    void Update()
    {
        timeBetweenWaves -= Time.deltaTime;
        //Debug.Log(timeBetweenWaves.ToString());
        Checktime();
        Canvas.ForceUpdateCanvases();
        CheckShop();
        CheckPlayerHP();
        SetTextUI();
        BuildManager.buildManager.UpdateGUI();
    }

    public void CheckShop()
    {
        int money = Money.GetMoney();
        if (money > BuildManager.buildManager.turrets[0].GetComponent<Tower>().towerCosts)
        {
            button[0].interactable = true;
        }
        else
        {
            button[0].interactable = false;
        }

        if (money > BuildManager.buildManager.turrets[1].GetComponent<Tower>().towerCosts)
        {
            button[1].interactable = true;
        }
        else
        {
            button[1].interactable = false;
        }

        if (BuildManager.buildManager.GetSelectedTower() !=null)
        {
            if (money >= BuildManager.buildManager.GetSelectedTower().GetComponent<Tower>().UpgradeCosts)

            {
                button[3].interactable = true;
            }
            if (money <= BuildManager.buildManager.GetSelectedTower().GetComponent<Tower>().UpgradeCosts)
            {
                button[3].interactable = false;
            }
        }
        if (BuildManager.buildManager.GetSelectedTower() == null)

        {
            button[3].interactable = false;
        }
    }

    void Checktime()
    {
        //NEW WAVE BEGINS HERE
        if (timeBetweenWaves <= 0f)
        {
            timeBetweenWaves = 5f;
            level++;
            SetTextUI();
            SetEnemyPossibility();
            StartCoroutine(Spawnwave());
        }
    }

    void SetTextUI()
    {
        roundUI.text = "Round :" + level.ToString();
        moneyUI.text = "Money :" + Money.GetMoney();
        //countdown.text = "Next Wave in: " + Mathf.Floor(timeBetweenWaves).ToString();
        lives.text = "Lives :" + Lives.GetLives().ToString();
    }

    IEnumerator Spawnwave()
    {
        //Standard unit is lvl1 unit
        int saverUnit = 0;
        int saverPossibilty = 100;
        System.Random randy = new System.Random();
        //This for gets the possibility for the first possible enemy
        for (int i = 0; i < possibilty.Length; i++)
        {
            if (possibilty[i] != 0)
            {
                saverUnit = i;
                saverPossibilty = possibilty[i];
                break;
            }
        }

        //As only 2 kind of units per wave can be spawned +1 spawns the "stronger" one
        for (int i = 0; i < level + 5; i++)
        {
            if (randy.Next(100) > saverPossibilty)
            {
                SpawnUnit(saverUnit + 1);
                //waits for the a float seconds, in this case the value of timeBetweenEnemies
                yield return new WaitForSeconds(timeBetweenEnemies);
            }
            else
            {
                SpawnUnit(saverUnit);
                //waits for the a float seconds, in this case the value of timeBetweenEnemies
                yield return new WaitForSeconds(timeBetweenEnemies);
            }
        }
    }

    void SpawnUnit(int unit)
    {
        //Instantiates a unit which is in the array of enemy prefabs by its index in the array
        Instantiate(enemies[unit], spawnPoint.position, spawnPoint.rotation);
    }
}

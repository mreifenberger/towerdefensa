﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{

    

    [Header("Objects")]
    public GameObject bulletImpactParticles;


    private Transform target;           //Tower passed Values
    private int bulletDamage;
    private string [] enemyTags;
    private float bulletSpeed = 0f;
    private float explosionRadius = 0;

    public string[] EnemyTags
    {
        get
        {
            return enemyTags;
        }

        set
        {
            enemyTags = value;
        }
    }
    public int BulletDamage
    {
        get
        {
            return bulletDamage;
        }

        set
        {
            bulletDamage = value;
        }
    }

    public float BulletSpeed
    {
        get
        {
            return bulletSpeed;
        }

        set
        {
            bulletSpeed = value;
        }
    }

    public float ExplosionRadius
    {
        get
        {
            return explosionRadius;
        }

        set
        {
            explosionRadius = value;
        }
    }

    public void Seek(Transform _target)         //Tower passed Target
    {
        target = _target;
    }

    // Update is called once per frame
    void Update()
    {

        if (target == null)             //destroy bullet if target disappears
        {
            Destroy(gameObject);
            return;
        }

        Vector3 dir = target.position - transform.position;     //get direktion to move 
        float distanceThisFrame = BulletSpeed * Time.deltaTime;

        if (dir.magnitude <= distanceThisFrame)
        {
            HitTarget();                                        //hit target
            return;
        }

        transform.Translate(dir.normalized * distanceThisFrame, Space.World); //Move
        transform.LookAt(target); 
    }

    void HitTarget()                                //if (Bullet position == target position)
    {
        Destroy(gameObject);

        GameObject enemy = (GameObject)target.gameObject;
        if (ExplosionRadius <= 0)
        {
            Damage(target);                         // Normal Tower
        }
        else
        {
            Explode(target);                        //Missile Tower
        }

        GameObject impactEffect = (GameObject)Instantiate(bulletImpactParticles, transform.position, transform.rotation);
        Destroy(impactEffect, 2f);
    }

    void Damage(Transform enemy)                    // Normal Tower
    {
        Enemyhp e = enemy.GetComponent<Enemyhp>();

        if (e != null)
        {
            e.HitByTower(bulletDamage);
        }
    }

    void Explode(Transform enemy)                    //Missile Tower
    {
        Enemyhp e = enemy.GetComponent<Enemyhp>();

        if (e != null)
        {
            Collider[] colliders=Physics.OverlapSphere(transform.position, ExplosionRadius);
            foreach (Collider collider  in colliders)
            {
                foreach (string tag in enemyTags)
                {
                    if (collider.tag == tag)
                    {
                        Damage(collider.transform);
                    }
                }
            }
        }
    }

    void OnDrawGizmosSelected()                      //Show Explosion Radius when selected
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, ExplosionRadius);
    }
}



﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemymovement : MonoBehaviour
{

    public int targetIndex = 0;
    private Transform target;
    float speed = 10f;

    private void Start()
    {
        target = Waypoints.waypoints[0];
    }

    private void Update()
    {
        CheckIndex();
        Move();
        CheckDistance();
    }

    private void GetNextWayPoint()
    {
        if (targetIndex != Waypoints.waypoints.Length-1)
        {
            targetIndex++;
            target = Waypoints.waypoints[targetIndex];
        }
        else
        {
            Lives.SetLives(1);
            Destroy(gameObject);
            return;
        }
    }



    private void CheckIndex()
    {
        if (targetIndex  >= Waypoints.waypoints.Length)
        {
            Lives.SetLives(1);
            Destroy(gameObject);
            return;
        }
    }

    private void Move()
    {
        Vector3 dir = target.position - transform.position;
        transform.Translate(dir.normalized * speed * Time.deltaTime, Space.World);
    }

    private void CheckDistance()
    {
        if (Vector3.Distance(transform.position, target.position) <= 0.4f)
        {
            GetNextWayPoint();
        }
    }

}

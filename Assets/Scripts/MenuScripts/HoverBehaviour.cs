﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HoverBehaviour : MonoBehaviour {

    public GameObject Text;
    public GameObject Underline;
    public Color defaultColor;
    public Color hoverColor;

    private void Start()
    {
        
    }

    private void Update()
    {
        
    }

    public void OnMouseEnter()
    {
        Underline.GetComponent<Image>().color = hoverColor;
    }

    public void OnMouseExit()
    {
        Underline.GetComponent<Image>().color = defaultColor;
    }

}

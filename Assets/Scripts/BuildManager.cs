﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class BuildManager : MonoBehaviour {

    [Header("Turrets")]
    public GameObject[] turrets;        //All Towers
    public Text towerLevelText;
    public Text towerDmgText;
    public Text towerRangeText;


    static public BuildManager buildManager;            //Singelton(To make the buildmanager accesable in the whole scene)
    private GameObject turretToBuild;                   //Selected Tower for building
    private GameObject selectedTower=null;                   //Selected Tower for upgrading

    void Awake()
    {
        if (buildManager == null)
        {
            buildManager = this;
        }
        else
            Debug.Log("More than one BuildManager");
    }

    public GameObject GetTurretToBuild()         
    {
        return turretToBuild;
    }

    public GameObject GetTurretToUpgrade()
    {
        return selectedTower;
    }

    public void SetTurretToBuild(GameObject turretToBuild)
    {
        this.turretToBuild = turretToBuild;
    }

    public void SetTurretToUpgrade(GameObject selectedTower)
    {
        this.selectedTower = selectedTower;
    }

    public GameObject GetSelectedTower()
    {
        return selectedTower;
    }

    public void UpdateGUI()
    {
        if (selectedTower != null)
        {
            Tower selectedT = selectedTower.GetComponent<Tower>();

            towerLevelText.text = "Tower-Level: " + selectedT.GetTowerLevel();
            towerDmgText.text = "Tower-Dmg: " + selectedT.towerDamage.ToString();
            towerRangeText.text = "Tower-Range: " + selectedT.range.ToString();
        }
    }
}

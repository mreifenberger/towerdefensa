﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemyhp : MonoBehaviour {


    private int hp;

	void Start () {
        InitializeHP();
        InvokeRepeating("UpdateTarget", 0f, 0.1f);
    }

    private void UpdateTarget()
    {
        if(hp<=0)
        {
            Money.SetMoney(GetMoney());
            Destroy(gameObject);
            return;
        }
    }

    private int GetMoney()
    {
        switch (gameObject.tag)
        {
            case "EnemyLvl1":
                {
                    return 5;
                }
            case "EnemyLvl2":
                {
                    return 7;
                }
            case "EnemyLvl3":
                {
                    return 10;
                }
            default:
                return 0;
        }
    }

    public void HitByTower(int damage)
    {
        this.hp -= damage;
    }

    private void InitializeHP()
    {
        switch(gameObject.tag)
        {
            case "EnemyLvl1":
                {
                    this.hp = 100;
                    break;
                }
            case "EnemyLvl2":
                {
                    this.hp = 200;
                    break;
                }
            case "EnemyLvl3":
                {
                    this.hp = 300;
                    break;
                }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoints : MonoBehaviour {

    // Use this for initialization
    public static Transform[] waypoints;
    private void Awake()
    {
        waypoints = new Transform[transform.GetChildCount()];
        for (int  i= 0;  i< waypoints.Length; i++)
        {
            waypoints[i] = transform.GetChild(i);
        }
    }
}

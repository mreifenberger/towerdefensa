﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Money : MonoBehaviour {

    private static int money = 100;

    public static int GetMoney()
    { return money; }

    public static void SetMoney(int _money)
    {
        money += _money;
    }
}

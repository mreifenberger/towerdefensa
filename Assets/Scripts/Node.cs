﻿using UnityEngine;

public class Node : MonoBehaviour
{

    [Header("Nodes Attributes")]
    public Color selectedColor;
    public Color hoverColor;
    public Vector3 offset;


    private GameObject turret;

    private Renderer rend;          //Hover Variables
    public Color startColor;

    void Start()
    {
        rend = GetComponent<Renderer>();
        startColor = rend.material.color;
    }

    private void Update()
    {
        if (turret == BuildManager.buildManager.GetSelectedTower()&& BuildManager.buildManager.GetSelectedTower()!=null)
        {
            rend.material.color = selectedColor;
        }
        if (turret != BuildManager.buildManager.GetSelectedTower() && rend.material.color != hoverColor)
        {
            rend.material.color = startColor;
        }
    }

    void OnMouseDown()
    {
        if (turret != null)                         //Already Tower on that node
        {
            //if (BuildManager.buildManager.upgrade == true)
            //{
            //    UpgradeInvoke();
            //}
            //if (BuildManager.buildManager.delete == true)
            //{
            //    Destroy(turret, 0f);
            //    BuildManager.buildManager.delete = false;
            //}
            BuildManager.buildManager.SetTurretToUpgrade(turret);

        }
        else
        {
            SetTower();
        }
    }

    void OnMouseEnter()
    {
        rend.material.color = hoverColor;
    }

    void OnMouseExit()
    {
        rend.material.color = startColor;
    }

    void SetTower()
    {
        GameObject turretToBuild = BuildManager.buildManager.GetTurretToBuild();            //Build tower
        if (turretToBuild != null && Money.GetMoney() >= turretToBuild.GetComponent<Tower>().towerCosts)
        {
            turret = (GameObject)Instantiate(turretToBuild, transform.position + offset, transform.rotation);
            Money.SetMoney(-turret.GetComponent<Tower>().towerCosts);
        }
    }
    //void UpgradeInvoke()
    //{
    //    turret.GetComponent<Tower>().Upgrade();
    //}
}

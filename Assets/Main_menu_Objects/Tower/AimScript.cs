﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimScript : MonoBehaviour {

    public GameObject bulletPrefab;
    public Transform firepoint;
    public GameObject shootParticle;
    public float speed;
    public float bulletSpeed;
    public float waitTime=2;
    private float wTime;

    bool exit=false;
    bool start=false;

    // Use this for initialization
    void Start () {
		
	}


    void FixedUpdate()
    {
        // Generate a plane that intersects the transform's position with an upwards normal.
        Plane playerPlane = new Plane(Vector3.up, transform.position);

        // Generate a ray from the cursor position
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        // Determine the point where the cursor ray intersects the plane.
        // This will be the point that the object must look towards to be looking at the mouse.
        // Raycasting to a Plane object only gives us a distance, so we'll have to take the distance,
        //   then find the point along that ray that meets that distance.  This will be the point
        //   to look at.
        float hitdist = 0.0f;
        // If the ray is parallel to the plane, Raycast will return false.
        if (playerPlane.Raycast(ray, out hitdist))
        {
            // Get the point along the ray that hits the calculated distance.
            Vector3 targetPoint = ray.GetPoint(hitdist);

            // Determine the target rotation.  This is the rotation if the transform looks at the target point.
            Quaternion targetRotation = Quaternion.LookRotation(targetPoint - transform.position);

            // Smoothly rotate towards the target point.
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, speed * Time.deltaTime);
        }
        if (start == true)
        {
            wTime -= Time.deltaTime;
            if (wTime <= 0)
            {
                Application.LoadLevel("Main");
            }
        }

        if (exit == true)
        {
            wTime -= Time.deltaTime;
            if (wTime <= 0)
            {
                Application.Quit();
            }
        }
    }
    public void Shoot()
    {
        GameObject spawnedBullet = (GameObject)Instantiate(bulletPrefab, firepoint.position, firepoint.rotation);
        spawnedBullet.GetComponent<Rigidbody>().AddForce(-bulletSpeed, 0, 0, ForceMode.Impulse);
        GameObject shootEffect = (GameObject)Instantiate(shootParticle, firepoint.position, firepoint.rotation);
        shootEffect.transform.parent = this.firepoint.transform;
        Destroy(shootEffect, 0.3f);
    }

    public void StartGame()
    {
        wTime = waitTime;
        start = true;
    }

    public void ExitGame()
    {
        wTime = waitTime;
        exit = true;
    }
}
